import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict


def default_SIR(graph):
    """SIR du prof

    Args:
        graph (dataframe): dataframe du graph

    Returns:
        array numpy: status[t,i] is the status of the node at time t
    """
    # Information
    # status[t,i] is the status of the node at time t

    ##################################################
    # Initialisation
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)

    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    # edt is a list of dictionary: {node_i: list_of_neighbours }
    # It is a different representation of the adjecency matrix
    edt = [defaultdict(list) for i in range(tmax+1)]
    for t, i, j in graph.values:
        edt[t][i].append(j)
        edt[t][j].append(i)  # undirected

    S, I, R = 0, 1, 2  # Let us define some states
    pI0 = 0.05  # Initial fraction of infected
    beta = 0.1  # contagion rate
    gamma = 0.01  # recovery rate

    ##################################################
    # Algo
    status = np.full((tmax+1, nb_node), S)

    status[0, rand(pI0, nb_node)] = I

    for t in range(tmax):

        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_contact = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == S], dtype=int)
        new_infected = risk_contact[rand(beta, len(risk_contact))]

        status[t+1, new_infected] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R
    ##################################################

    return status


def default_propagation(graph):
    """Default Propagation (celle du prof)

    Args:
        graph (dataframe): dataframe du graph

    Returns:
        array numpy: status[t,i] is the status of the node at time t
    """
    return default_SIR(graph)


def afficher_default_propagation(status):
    """Cette fonction a principalement pour but de vérifier que le programme par défault fonctionne bien

    Args:
        status (array numpy): status[t,i] is the status of the node at time t
    """
    S, I, R = 0, 1, 2  # Let us define some states
    plt.plot((status == I).mean(axis=1))
    plt.xlabel('time (20 sec)', fontsize=16)
    plt.ylabel('Infected', fontsize=16)
    plt.tick_params(labelsize=14)
    plt.show()
