import numpy as np
from collections import defaultdict


def propagation(graph, pI0, beta, alpha, gamma, mu):
    """Modèle SEIR avec deux types d'infections détectés et non détecté.

    Args:
        graph
        pI0 (float): proportion de malade à t=0
        beta (float): taux de contagiosité
        alpha (float): taux d'exposition
        gamma (float): taux de recover
        mu (float): taux de mort
    """
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)
    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    edt = [defaultdict(list) for i in range(tmax+1)]

    for t, i, j in graph.values:
        edt[t][i].append(j)
        edt[t][j].append(i)

    # S=Sain, E=Exposed, I=Infected, R=Recovered, D=Dead
    S, I, R, E, D = 0, 1, 2, 3, 4

    status = np.full((tmax+1, nb_node), S)
    status[0, rand(pI0, nb_node)] = I

    for t in range(tmax):

        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_infection = np.where(status[t] == E)[0]
        risk_contact = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == S], dtype=int)

        new_exposed = risk_contact[rand(alpha, len(risk_contact))]
        status[t+1, new_exposed] = E
        new_infected = risk_infection[rand(beta, len(risk_infection))]
        status[t+1, new_infected] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R
        new_dead = infected[rand(mu, len(infected))]
        status[t+1, new_dead] = D

    return status
