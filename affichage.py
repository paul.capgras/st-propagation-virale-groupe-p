import numpy as np
import igraph as ig


def afficher_graph(graph, t):
    """Pour un instant t donné, affiche le graph en question

    Args:
        graph (dataframe): dataframe du graph
        t (int): temps pour lequel on veut connaitre l'état du graph
    """
    nb_node = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    ed100 = graph.query('time=='+str(t))
    g = ig.Graph(edges=ed100[['node-i', 'node-j']].values.tolist())
    g.vs['label'] = np.arange(0, nb_node)
    if 'weight' in ed100:
        visual_style = {}
        visual_style['edge_width'] = (ed100['weight'].values/100*5).tolist()
        return ig.plot(g, **visual_style)
    else:
        return ig.plot(g)


def afficher_status(graph, status, t):
    """Affiche avec des couleurs le graph à l'instant t:
    Noir = Recover
    Gris = Safe (exposed)
    Nouge = Infected
    Vert = Vaccinated
    Args:
        graph
        status (array numpy): status[t,i] is the status of the node at time t
        t (int):temps d'affichage
    """
    nb_node = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    ed_t = graph.query('time=='+str(t))
    g = ig.Graph(edges=ed_t[['node-i', 'node-j']
                            ].values.tolist())
    g.vs['label'] = np.arange(0, nb_node)
    color_dict = {0: "white", 1: "red", 2: "gray",
                  3: "orange", 4: 'black', 5: 'green'}
    g.vs["color"] = [color_dict[statut] for statut in status[t]]
    if 'weight' in ed_t:
        visual_style = {}
        visual_style['edge_width'] = (ed_t['weight'].values/100*5).tolist()
        return ig.plot(g, **visual_style)
    else:
        return ig.plot(g)
