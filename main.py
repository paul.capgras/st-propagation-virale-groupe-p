from algo_propagation import *
from generation_graph import *
from vaccination import *
import igraph as ig
from affichage import *
from vaccination.vaccination_SEIR import vaccination_SEIR


def main():
    """
    pI0 = 0.05  # Initial fraction of infected
    beta = 0.1  # contagion rate
    gamma = 0.01  # recovery rate
    """
    graph = generate_partition_graph(8, 10, 5, p=0.2, pf=1.0)
    afficher_graph(graph, 0)
    """
    pI0 = 0.1  # Initial fraction of infected
    beta = 0.4  # contagion rate
    gamma = 0.2  # recovery rate
    graph = generate_weight_graph(20, 100)
    alpha = 0.3
    mu = 0.2
    status = vaccination_SEIR(graph, pI0, beta, alpha, gamma, mu, 0.8, 0.95, 2)
    tmax = len(status)
    for t in range(int(tmax)):
        afficher_status(graph, status, t)"""


if __name__ == '__main__':
    main()
