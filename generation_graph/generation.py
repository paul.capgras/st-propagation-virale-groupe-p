import pandas as pd
import numpy as np
import igraph as ig
import matplotlib.pyplot as plt
import random as rd


def generate_weight_graph(nb_node, t_max, p=0.2):
    """Cette fonction génère un graphe 
    où les noeuds sont connectés aléatoirements
    entre eux avec une propabilité p
    Chaque edge est doté d'un poid : 100 le risque de contagion est très elevé, 0 presque nul

    Args:
        nb_node (int): nombre maximal de noeud. Donc les noeuds sont numérotés de 0 à nb_node-1
        t_max (int): temps maximal. Donc t appartient à [0, tmax]
        p (float): probabilité que deux noeuds soient connectées entre eux

    Returns:
        dataframe: graph aléatoire au format time, node-i, node-j qui décrit les connexions
    """
    graph = pd.DataFrame(columns=['time', 'node-i', 'node-j', 'weight'])

    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    for t in range(t_max+1):
        for node in range(nb_node-1):
            liste_voisin = []
            random = rand(p, nb_node - node - 1)
            for i in range(len(random)):
                if random[i]:
                    weight = int(rd.random()*100)
                    liste_voisin.append([t, node, node+i+1, weight])
                    liste_voisin.append([t, node+i+1, node, weight])
            arr = np.array(liste_voisin)
            if len(arr) > 0:
                graph_node = pd.DataFrame(
                    arr, columns=['time', 'node-i', 'node-j', 'weight'])
                if graph.empty:
                    graph = graph_node
                else:
                    graph = pd.concat([graph, graph_node], ignore_index=True)

    return graph


def generate_partition_graph(nb_partition, t_max, nb_elements=10, p=0.2, pf=0.8):
    """Genère un graph avec des poids et des familles

    Args:
        nb_partition (int): nombre de familles. max 999
        t_max (int): temps
        nb_elements (int, optional): nombre moyen de noeud dans une famille. Defaults to 10. Maximum 999
        p (int, optional): connection entre famille. Defaults to 0.2.
        pf (int, optional): connnection dans la famille. Defaults to 0.8
    """
    assert nb_partition < 1000
    assert nb_elements < 1000

    graph_final = pd.DataFrame(
        columns=['time', 'node-i', 'node-j', 'weight'])
    nb_node_par_famille = []
    nb_node_total = 0
    for i in range(0, nb_partition):

        taille_famille = int(rd.random()*nb_elements)

        nb_node_par_famille.append(taille_famille)
        nb_node_total += taille_famille

        arr_famille = generate_weight_graph(taille_famille, t_max, pf).values
        arr_famille[:, 1] += np.array([nb_node_total]*len(arr_famille))
        arr_famille[:, 2] += np.array([nb_node_total]*len(arr_famille))
        # graph_famille['node-i'], graph_famille['node-j'] += nb_node_total, nb_node_total
        if len(arr_famille) > 0:
            graph_famille = pd.DataFrame(
                arr_famille, columns=['time', 'node-i', 'node-j', 'weight'])
            if graph_final.empty:
                graph_final = graph_famille
            else:
                graph_final = pd.concat(
                    [graph_final, graph_famille], ignore_index=True)
    raise NotImplementedError


def generate_random_graph(nb_node, t_max, p=0.2):
    """Cette fonction génère un graphe 
    où les noeuds sont connectés aléatoirements
    entre eux avec une propabilité p

    Args:
        nb_node (int): nombre maximal de noeud. Donc les noeuds sont numérotés de 0 à nb_node-1
        t_max (int): temps maximal. Donc t appartient à [0, tmax]
        p (float): probabilité que deux noeuds soient connectées entre eux

    Returns:
        dataframe: graph aléatoire au format time, node-i, node-j qui décrit les connexions
    """
    graph = pd.DataFrame(columns=['time', 'node-i', 'node-j'])

    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    for t in range(t_max+1):
        for node in range(nb_node-1):
            liste_voisin = []
            random = rand(p, nb_node - node - 1)
            for i in range(len(random)):
                if random[i]:
                    liste_voisin.append([t, node, node+i+1])
                    liste_voisin.append([t, node+i+1, node])
            arr = np.array(liste_voisin)
            if len(arr) > 0:
                graph_node = pd.DataFrame(
                    arr, columns=['time', 'node-i', 'node-j'])
                if graph.empty:
                    graph = graph_node
                else:
                    graph = pd.concat([graph, graph_node], ignore_index=True)

    return graph


def test_generate_graph(graph):
    """
    Cette fonction vérifie brièvement que le format du graph est bon
    Si une erreur apparait quand vous l'executez depuis le main, posez vous des questions
    """
    t = graph['time']
    edges = graph[['node-i', 'node-j']].values.tolist()
