import pandas as pd
import igraph as ig
import matplotlib.pyplot as plt


def default_generate_graph():
    """Genere un graph.
    Description du format du graph:
    C'est une dataframe pandas avec trois colonne time, node-i, node-j
    Chaque ligne correspond à une arête à un instant t. Le graph évolue donc dans le temps
    Returns:
        dataFrame
    """
    # dataframe pandas qui décrit le graph
    graph_description = pd.read_csv(
        'generation_graph/default_graph.csv')
    return graph_description


def afficher_default_graph(graph, t):
    """Pour un instant t donné, affiche le graph en question

    Args:
        graph (dataframe): dataframe du graph
        t (int): temps pour lequel on veut connaitre l'état du graph
    """
    ed100 = graph.query('time=='+str(t))
    g = ig.Graph(edges=ed100[['node-i', 'node-j']].values.tolist())
    ig.plot(g, vertex_size=5)
    plt.show()
