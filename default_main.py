from algo_propagation import *
from generation_graph import *


def default_main():

    graph = default_generate_graph()
    afficher_default_graph(graph, 100)

    status = default_propagation(graph)
    afficher_default_propagation(status)


if __name__ == '__main__':
    default_main()
