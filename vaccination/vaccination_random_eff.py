import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import igraph as ig


# Now, we are going to still vaccinate randomly people with a defined proportion, but we are going to change the full
# immunity given to vaccinated people.

def vaccination_random_eff(graph, pI0, beta, gamma, pV0, efficiency):
    """We introduce a new parameter : the efficiency of the vaccine.

    Args:
        graph (dataframe): dataframe du graph
        pI0 : Initial fraction of infected
        beta : contagion rate
        gamma : recovery rate
        pv0 : initial proportion of vaccinated people
        efficiency : power of the vaccination : pourcentage of immunity that it gives to people

    Returns:
        array numpy: status[t,i] is the status of the node at time t
    """
    # Information
    # status[t,i] is the status of the node at time t

    ##################################################
    # Initialisation
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)

    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    # edt is a list of dictionary: {node_i: list_of_neighbours }
    # It is a different representation of the adjecency matrix
    edt = [defaultdict(list) for i in range(tmax+1)]
    for t, i, j in graph.values:
        edt[t][i].append(j)
        edt[t][j].append(i)  # undirected

    S, I, R, V = 0, 1, 2, 3  # Let us define some states : V is a new state

    ##################################################
    # Algo
    status = np.full((tmax+1, nb_node), S)

    status[0, rand(pV0, nb_node)] = V
    status[0, rand(pI0, nb_node)] = I

    for t in range(tmax):

        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_contact_1 = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == S], dtype=int)
        risk_contact_2 = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == V], dtype=int)
        new_infected_1 = risk_contact_1[rand(beta, len(risk_contact_1))]
        new_infected_2 = risk_contact_2[rand(
            1-efficiency, len(risk_contact_2))]
        status[t+1, new_infected_1] = I
        status[t+1, new_infected_2] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R
    ##################################################

    return status


def afficher_vaccination_propagation_random_eff(status, pV0, efficiency):
    """
    Args:
        status (array numpy): status[t,i] is the status of the node at time t
        pv0 : initial proportion of vaccinated people
        efficiency : power of the vaccination : pourcentage of immunity that it gives to people
    """
    S, I, R = 0, 1, 2  # Let us define some states
    plt.plot((status == I).mean(axis=1))
    plt.xlabel('time (20 sec)', fontsize=16)
    plt.ylabel('Infected', fontsize=16)
    plt.title('Evolution of the number of infected with ' +
              str(pV0*100) + ' % of vaccinated people with an efficiency of the vaccine of ' + str(efficiency*100) + '%')
    plt.tick_params(labelsize=14)
    plt.show()


# Now we are going to simulate the previous fonction multiple thousand times in order to be able to get an average
# evolution of the number of vaccinated people.

def vaccination_random_eff_mean(graph, pI0, beta, gamma, pV0, efficiency, N):
    """We introduce a new parameter : N as we simulate the propagation of the epidemic N times to get the mean of
    all the simulations.

    Args:
        graph (dataframe): dataframe du graph
        pI0 : Initial fraction of infected
        beta : contagion rate
        gamma : recovery rate
        pv0 : initial proportion of vaccinated people
        efficiency : power of the vaccination : pourcentage of immunity that it gives to people
        N : number of times the simulation is repeated

    Returns:
        array numpy: status[t,i] is the status of the node at time t"""

    tmax = max(graph['time'].values)
    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1
    S, I, R, V = 0, 1, 2, 3
    status = np.full((tmax+1, nb_node), S)
    for i in range(N):
        status += vaccination_random_eff(graph,
                                         pI0, beta, gamma, pV0, efficiency)
    status = status/N
    return status


# Now we are going to plot the mean evolution of the number of infected people for different values of the parameter
# efficiency, to see the impact of the efficiency of the vaccine on epidemics.


def afficher_vaccination_propagation_random_multiple_efficiency_mean(graph, pI0, beta, gamma, pV0, N, rep):
    """This function compares the evolution of the mean number of infected for different values of the efficiency
    of the vaccine. We introduce rep, the number of values of the efficiency we want to have.

    Args:
        graph
        pI0 : Initial fraction of infected
        beta : contagion rate
        gamma : recovery rate
        pv0: initial proportion of vaccinated people
        N : number of times the simulation is repeated
        rep : number of different efficiencies tested
    """
    S, I, R = 0, 1, 2  # Let us define some states
    efficiency = 0
    for i in range(rep):
        status = vaccination_random_eff_mean(
            graph, pI0, beta, gamma, pV0, efficiency, N)
        plt.plot((status == I).mean(axis=1))
        if rep-1 != 0:
            efficiency += 1/(rep-1)
    plt.legend(np.linspace(0, 1, rep))
    plt.xlabel('time (20 sec)', fontsize=16)
    plt.ylabel('Infected', fontsize=16)
    plt.title('Evolution of the number of infected with ' +
              str(pV0*100) + ' % of vaccinated people with different efficiences of the vaccine repeated ' + str(N) + ' times')
    plt.tick_params(labelsize=14)
    plt.show()
