import numpy as np
from collections import defaultdict
import igraph as ig
import random as rd


def step_vaccination(graph, status, S, t, node_max, delta, nb_vac_max):
    """A step of vaccination

    Args:
        graph
        status
        S
        t
        node_max
        delta : pourcentage of the most connected node that are vaccined
        nb_vac_max : nombre de vacciné maximal par manche

    Returns:
        [list]: liste des noeuds à vacciner
    """
    # It returns a list of the different degrees of the nodes.
    grapht = graph.query('time=='+str(t+1))
    g = ig.Graph(edges=grapht[['node-i', 'node-j']].values.tolist())
    g.vs['label'] = np.arange(0, node_max)
    nb_degree = g.degree()
    nodes_connect = []
    for i in range(len(nb_degree)):
        if nb_degree[i] > delta*max(nb_degree) and status[t, i] == S:
            nodes_connect.append(i)
        if len(nodes_connect) == nb_vac_max:
            break
    return nodes_connect


def vaccination_SEIR(graph, pI0, beta, alpha, gamma, mu, efficiency, delta, nb_vac_max):
    """Modèle SEIR avec deux types d'infections détectés et non détecté.

    Args:
        graph avec des liens avec poids
        pI0 (float): proportion de malade à t=0
        beta (float): taux de contagiosité
        alpha (float): taux d'exposition
        gamma (float): taux de recover
        mu (float): taux de mort
        efficiency : power of the vaccination : pourcentage of immunity that it gives to people
        delta : pourcentage of the most connected node that are vaccined
        nb_vac_max : nombre de vacciné maximal par manche
    """
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)
    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    edt = [defaultdict(list) for i in range(tmax+1)]

    for t, i, j, w in graph.values:
        edt[t][i].append((j, w))
        edt[t][j].append((i, w))

    # S=Sain, E=Exposed, I=Infected, R=Recovered, D=Dead
    S, I, R, E, D, V = 0, 1, 2, 3, 4, 5

    status = np.full((tmax+1, nb_node), S)

    # Vaccination
    nodes_connect = step_vaccination(
        graph, status, S, 0, node_max, delta, nb_vac_max)
    status[0, nodes_connect] = V

    status[0, rand(pI0, nb_node)] = I

    for t in range(tmax):

        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_infection = np.where(status[t] == E)[0]

        risk_contact_1 = np.array(
            [j for i in infected for j, w in edt[t][i] if status[t, j] == S and rd.random() < w], dtype=int)
        risk_contact_2 = np.array(
            [j for i in infected for j, w in edt[t][i] if status[t, j] == V and rd.random() < w], dtype=int)
        new_infected_1 = risk_contact_1[rand(beta, len(risk_contact_1))]
        new_infected_2 = risk_contact_2[rand(
            1-efficiency, len(risk_contact_2))]
        status[t+1, new_infected_1] = E
        status[t+1, new_infected_2] = E

        new_infected = risk_infection[rand(beta, len(risk_infection))]
        status[t+1, new_infected] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R
        new_dead = infected[rand(mu, len(infected))]
        status[t+1, new_dead] = D

        # Vaccination
        nodes_connect = step_vaccination(
            graph, status, S, t, node_max, delta, nb_vac_max)
        status[t+1, nodes_connect] = V
    return status
