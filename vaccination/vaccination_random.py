# Paramétrage de la vaccination

# First, we are going to modify the default propagation algorithm given in class, in order to test some strategies
# of vaccination before applying them to the new propagation version.

# The first strategy is to vaccinate a proportion of people randomly. Vaccination will for the moment give a full
# immunity against the disease.
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import igraph as ig


def vaccination_random(graph, pI0, beta, gamma, pV0):
    """New model : SIRV considering the vaccinated people differently than the recovered people, in order to
    limit the power of vaccination in a second place and make people fell sick while being vaccinated.

    Args:
        graph (dataframe): dataframe du graph
        pI0 : Initial fraction of infected
        beta : contagion rate
        gamma : recovery rate
        pv0 : initial proportion of vaccinated people

    Returns:
        array numpy: status[t,i] is the status of the node at time t
    """
    # Information
    # status[t,i] is the status of the node at time t

    ##################################################
    # Initialisation
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)

    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    # edt is a list of dictionary: {node_i: list_of_neighbours }
    # It is a different representation of the adjecency matrix
    edt = [defaultdict(list) for i in range(tmax+1)]
    for t, i, j in graph.values:
        edt[t][i].append(j)
        edt[t][j].append(i)  # undirected

    S, I, R, V = 0, 1, 2, 3  # Let us define some states : V is a new state

    ##################################################
    # Algo
    status = np.full((tmax+1, nb_node), S)

    status[0, rand(pV0, nb_node)] = V
    status[0, rand(pI0, nb_node)] = I
    for t in range(tmax):

        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_contact = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == S], dtype=int)
        new_infected = risk_contact[rand(beta, len(risk_contact))]

        status[t+1, new_infected] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R
    ##################################################

    return status


def afficher_vaccination_propagation_random(status, pV0):
    """This function returns the simulation of the number of infected people.

    Args:
        status (array numpy): status[t,i] is the status of the node at time t
        pv0 : initial proportion of vaccinated people
    """
    S, I, R = 0, 1, 2  # Let us define some states
    plt.plot((status == I).mean(axis=1))
    plt.xlabel('time (20 sec)', fontsize=16)
    plt.ylabel('Infected', fontsize=16)
    plt.title('Evolution of the number of infected with ' +
              str(pV0*100) + ' % of vaccinated people')
    plt.tick_params(labelsize=14)
    plt.show()
