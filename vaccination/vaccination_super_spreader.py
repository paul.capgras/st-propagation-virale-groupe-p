import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
import igraph as ig

# Now we are going to use the same strategy as before, but implementing the vaccination of the most connected people
# at every t.


def vaccination_strategy_2_complexe(graph, pI0, beta, gamma, efficiency):
    """We improve the previous strategy by vaccinating at every time the potential super spreaders.

    Args:
        graph (dataframe): dataframe du graph
        pI0 : Initial fraction of infected
        beta : contagion rate
        gamma : recovery rate
        efficiency : power of the vaccination : pourcentage of immunity that it gives to people

    Returns:
        array numpy: status[t,i] is the status of the node at time t
    """
    # Information
    # status[t,i] is the status of the node at time t

    ##################################################
    # Initialisation
    def rand(x, n): return np.random.uniform(0, 1, size=n) < x

    tmax = max(graph['time'].values)

    node_max = max(np.max(graph['node-i'].values), np.max(graph['node-j']))+1
    nb_node = node_max + 1

    # edt is a list of dictionary: {node_i: list_of_neighbours }
    # It is a different representation of the adjecency matrix
    edt = [defaultdict(list) for i in range(tmax+1)]
    for t, i, j in graph.values:
        edt[t][i].append(j)
        edt[t][j].append(i)  # undirected

    S, I, R, V = 0, 1, 2, 3  # Let us define some states : V is a new state
    ##################################################
    # Algo
    status = np.full((tmax+1, nb_node), S)
    # It returns a list of the different degrees of the nodes.
    ed0 = graph.query('time=='+str(0))
    g = ig.Graph(edges=ed0[['node-i', 'node-j']
                           ].values.tolist())
    g.vs['label'] = np.arange(0, node_max)
    nb_degree = g.degree()
    nodes_connect = []
    for i in range(len(nb_degree)):
        if nb_degree[i] == max(nb_degree):
            nodes_connect.append(i)

    status[0, nodes_connect] = V
    status[0, rand(pI0, nb_node)] = I

    for t in range(tmax):
        status[t+1] = status[t]
        infected = np.where(status[t] == I)[0]
        risk_contact_1 = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == S], dtype=int)
        risk_contact_2 = np.array(
            [j for i in infected for j in edt[t][i] if status[t, j] == V], dtype=int)
        new_infected_1 = risk_contact_1[rand(beta, len(risk_contact_1))]
        new_infected_2 = risk_contact_2[rand(
            1-efficiency, len(risk_contact_2))]
        status[t+1, new_infected_1] = I
        status[t+1, new_infected_2] = I
        new_recovered = infected[rand(gamma, len(infected))]
        status[t+1, new_recovered] = R

        grapht = graph.query('time=='+str(t+1))
        g = ig.Graph(edges=grapht[['node-i', 'node-j']].values.tolist())
        g.vs['label'] = np.arange(0, node_max)
        nb_degree = g.degree()
        nodes_connect = []
        for i in range(len(nb_degree)):
            if nb_degree[i] > 0.80*max(nb_degree):
                nodes_connect.append(i)
        for node in nodes_connect:
            status[t+1, node] = V
    ##################################################

    return status


def afficher_vaccination_propagation_strategy_2_complexe(status, efficiency):
    """
    Args:
        status(array numpy): status[t, i] is the status of the node at time t
        efficiency: power of the vaccination: pourcentage of immunity that it gives to people
    """
    S, I, R = 0, 1, 2  # Let us define some states
    plt.plot((status == I).mean(axis=1))
    plt.xlabel('time (20 sec)', fontsize=16)
    plt.ylabel('Infected', fontsize=16)
    plt.title('Evolution of the number of infected with the most connected people at t = 0 vaccinated with an efficiency of the vaccine of ' +
              str(efficiency*100))
    plt.tick_params(labelsize=14)
    plt.show()
